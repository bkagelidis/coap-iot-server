CONTIKI_PROJECT = coap-example-server
all: $(CONTIKI_PROJECT)

# Do not try to build on Sky because of code size limitation
PLATFORMS_EXCLUDE = sky z1

# Include CoAP resources
MODULES_REL += ./resources

CONTIKI=../../..

# Include the CoAP implementation
include $(CONTIKI)/Makefile.dir-variables
MODULES += $(CONTIKI_NG_APP_LAYER_DIR)/coap

include $(CONTIKI)/Makefile.include

### Will allow the inclusion of the correct CPU makefile
CPU_FAMILY = cc26x0

### Add to the source dirs
CONTIKI_TARGET_DIRS += launchpad/cc2650

### Include the common launchpad makefile
include $(PLATFORM_ROOT_DIR)/launchpad/Makefile.launchpad